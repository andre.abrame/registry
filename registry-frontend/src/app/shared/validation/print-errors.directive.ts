import { Directive, ElementRef, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, NgModel, AbstractControl, ValidationErrors } from '@angular/forms';
import { Subscription, startWith } from 'rxjs';


function defaultMessage(key: string, error: ValidationErrors) {
  switch (key) {
    case 'required': return "Required field";
    case 'minlength': return `Too short. Min length is ${error['requiredLength']}, add ${error['requiredLength']-error['actualLength']} more character${error['requiredLength']-error['actualLength']>1 ? 's' : ''}`;
    case 'maxlength': return `Too long. Max length is ${error['requiredLength']}, remove ${error['actualLength']-error['requiredLength']} character${error['actualLength']-error['requiredLength']>1 ? 's' : ''}`;
    case 'email': return 'Email is not valid';
    case 'min': return `Min value is ${error['min']}, actual value is ${error['actual']}`;
    case 'max': return `Max value is ${error['max']}, actual value is ${error['actual']}`;
    case 'pattern': return 'Invalid value';
    case 'passwordMismatch': return 'Passwords do not match';
    default: return "error";
  }
}


@Directive({
  selector: '[printErrors]'
})
export class PrintErrorsDirective implements OnInit, OnDestroy {

  @Input("printErrors") fc?: FormControl | NgModel | AbstractControl | null;
  private subscription?: Subscription;

  constructor(private element: ElementRef) { }

  ngOnInit(): void {
    if (this.fc instanceof NgModel)
      this.fc = this.fc.control;
    if (this.fc) {
      this.subscription = this.fc.valueChanges
        .pipe(startWith(this.fc.value))
        .subscribe(v => {
          if (this.fc?.errors) {
            this.element.nativeElement.innerText = "";
            Object.keys(this.fc?.errors).forEach(k => {
              this.element.nativeElement.innerText += this.fc?.errors?.[k].message ?? defaultMessage(k, this.fc?.errors?.[k]);
            });
          }
        })
    }
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

}

