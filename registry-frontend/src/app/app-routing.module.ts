import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserListComponent } from './components/users/user-list/user-list.component';
import { UserAddComponent } from './components/users/user-add/user-add.component';
import { UserEditComponent } from './components/users/user-edit/user-edit.component';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  { path: "users", component: UserListComponent},
  { path: "users/add", component: UserAddComponent},
  { path: "users/:id/edit", component: UserEditComponent},
  { path: "login", component: LoginComponent},
  { path: "**", redirectTo: "users"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
