import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent {

  userFormGroup = User.formGroup();

  constructor(
    private userService: UserService,
    private router: Router
  ) {}

  onReset() {
    this.userFormGroup.setValue(User.empty);
    this.userFormGroup.markAsPristine();
    this.userFormGroup.markAsUntouched();
  }

  onSubmit() {
    if (this.userFormGroup.invalid)
      this.userFormGroup.markAsTouched();
    else
      this.userService
        .save(this.userFormGroup.value as User)
        .subscribe(
          () => { this.router.navigateByUrl("/users"); }
        );
  }

}
