import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  email: string = "";
  password: string = "";

  error: string = "";

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {}

  onLogin() {
    console.log(this.email);
    console.log(this.password);
    this.authenticationService.login(this.email, this.password).subscribe({
      next: () => { this.router.navigateByUrl("/users") },
      error: (err) => this.error = err
    })
  }

}
