import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";


export abstract class GenericService<T extends {id: number}> {

  constructor(
    private httpClient: HttpClient,
    protected url: string
  ) { }

  findAll(): Observable<T[]> {
    return this.httpClient.get<T[]>(this.url);
  }

  findById(id: number): Observable<T> {
    return this.httpClient.get<T>(this.url + "/" + id);
  }

  save(element: T): Observable<T> {
    return this.httpClient.post<T>(this.url, element);
  }

  update(element: T): Observable<void> {
    return this.httpClient.put<void>(this.url + "/" + element.id, element);
  }

  delete(element: T): Observable<void> {
    return this.httpClient.delete<void>(this.url + "/" + element.id);
  }
}
