import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { City } from '../models/city';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root'
})
export class CityService extends GenericService<City> {

  constructor(
    httpClient: HttpClient
  ) {
    super(httpClient, environment.backendUrl + "/cities");
  }
}
