import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import jwt_decode from 'jwt-decode'

export interface JwtResponse {
  accessToken: string;
  refreshToken: string;
}

export interface AuthenticatedUser {
  id: number;
  username: string;
  accessToken: string;
  refreshToken: string;
}


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  authenticatedUser: BehaviorSubject<AuthenticatedUser | undefined> = new BehaviorSubject<AuthenticatedUser | undefined>(undefined);

  constructor(
    private httpClient: HttpClient
  ) { }

  login(username: string, password: string): Observable<JwtResponse> {
    return this.httpClient.post<JwtResponse>(environment.backendUrl + "/authenticate", {
      grantType: "password",
      username: username,
      password: password
    }).pipe(
      tap(jr => {
        const decoded: any = jwt_decode(jr.accessToken);
        this.authenticatedUser.next({
          id: decoded.sub,
          username: decoded.name,
          accessToken: jr.accessToken,
          refreshToken: jr.refreshToken
        })
        sessionStorage.setItem("accessToken", jr.accessToken);
        sessionStorage.setItem("refreshToken", jr.refreshToken);
      })
    )
  }

  logout() {
    this.authenticatedUser.next(undefined);
    sessionStorage.removeItem("accessToken");
    sessionStorage.removeItem("refreshToken");
  }

}
