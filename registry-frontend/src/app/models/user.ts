import { FormControl, FormGroup, Validators } from "@angular/forms";
import { City } from "./city";


export interface User {
  id: number;
  name: string;
  email: string;
  password: string;
  cities: City[];
}

export namespace User {
  export const empty = { id: 0, name: '', email: '', password: '', cities: [] };

  export function formGroup(user: User = empty) {
    return  new FormGroup({
      id: new FormControl(user.id),
      name: new FormControl(user.name, [ Validators.required, Validators.minLength(3) ]),
      email: new FormControl(user.email, [ Validators.required, Validators.email ]),
      password: new FormControl(user.password),
      cities: new FormControl(user.cities)
    });
  }
}
