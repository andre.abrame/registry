package fr.formation.registry.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.registry.models.City;
import fr.formation.registry.repositories.CityRepository;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/cities")
@AllArgsConstructor
public class CityController {
    
    private CityRepository cityRepository;

    @GetMapping()
    List<City> findAll() {
        return cityRepository.findAll();
    }

    @GetMapping("{id}")
    City findById(@PathVariable long id) {
        return cityRepository.findById(id)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "no city with id " + id + " exist"));
    }

    @PostMapping()
    City save(@RequestBody City city) {
        return cityRepository.save(city);
    }

    @PutMapping("{id}")
    void update(@PathVariable long id, @RequestBody City city) {
        city.setId(id);
        if (cityRepository.findById(id).isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "no city with id " + id + " exist");
        cityRepository.save(city);
    }

    @DeleteMapping("{id}")
    void deleteBy(@PathVariable long id) {
        cityRepository.deleteById(id);
    }
}
